#!/usr/bin/bash

gradle_version=7.1.1
node_version=v18.19.0
android_sdk_version=9477386
platform_version=32
build_tools_version=32.0.0
ndk_version=21.0.6113669
cmake_version=3.22.1
docker_tag=node18-gradle${gradle_version}-jdk11

\cp -f "../assets/gradle-$gradle_version-all.tar.gz" .

docker build \
  --build-arg node_version=$node_version \
  --build-arg android_sdk_version=$android_sdk_version \
  --build-arg platform_version=$platform_version \
  --build-arg build_tools_version=$build_tools_version \
  --build-arg ndk_version=$ndk_version \
  --build-arg cmake_version=$cmake_version \
  --build-arg gradle_version=$gradle_version \
  -t linfulong/quasar-builder:$docker_tag .

rm -f "gradle-$gradle_version-all.tar.gz"
