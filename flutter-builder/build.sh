#!/bin/bash

# 指定镜像名称，格式为user name/image name
name=linfulong/flutter-builder
tag=latest

# 编译镜像，不使用缓存，则添加--no-cache
docker build -t $name:$tag .

# 需要先登录，再发布到docker hub，以便使用docker pull命令下载
# docker login
# docker push $name:$tag
